package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code new RockPaperScissors(). Then it calls the run()
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        Scanner validInput = new Scanner(System.in);

        while(true) {
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String moveMine = validInput.nextLine();

            //Making sure that the input is valid, the one that you want
            if(!moveMine.equals("rock") && !moveMine.equals("paper") && !moveMine.equals("scissors")){
                System.out.println("I do not understand cardboard. Could you try again?");
            }
            else{
                int randomise = (int)(Math.random() * 3);
                String moveOpponent = "";
                if (randomise == 0){
                    moveOpponent= "rock";
                }
                else if (randomise == 1){
                    moveOpponent = "paper";
                }
                else{
                    moveOpponent = "scissors";
                }

                if(moveMine.equals(moveOpponent)){
                    System.out.println("Human chose " + moveMine + ", computer chose "+ moveOpponent + ". It's a tie!");
                }
                else if((moveMine.equals("rock") && moveOpponent.equals("scissors"))
                        | (moveMine.equals("scissors") && moveOpponent.equals("paper"))
                        | (moveMine.equals("paper") && moveOpponent.equals("rock")) ){
                    System.out.println("Human chose " + moveMine + ", computer chose "+ moveOpponent + ". Human wins!");
                    humanScore += 1;
                }
                else{
                    System.out.println("Human chose " + moveMine + ", computer chose "+ moveOpponent + ". Computer wins!");
                    computerScore += 1;
                }
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            roundCounter += 1;

            //Continue answer
            System.out.println("Do you wish to continue playing? (y/n)?");
            String answer = validInput.nextLine();

            if(answer.equals("y")){
                continue;
            }
            else if(answer.equals("n")){
                System.out.println("Bye bye :)");
                break;
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}